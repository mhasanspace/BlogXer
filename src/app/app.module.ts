import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AuthComponentComponent } from './Authentication/auth-component/auth-component.component';
import { ResetpasswordComponent } from './Authentication/resetpassword/resetpassword.component';
import { VerifyMailComponent } from './Authentication/verify-mail/verify-mail.component';
import { NoSanitizePipe } from './Authentication/shared/no-sanitize.pipe';
import { HomeComponent } from './Unauthenticated/home/home.component';
import { UCreatePostComponent } from './Unauthenticated/u-create-post/u-create-post.component';
import { UpostDetailComponent } from './Unauthenticated/upost-detail/upost-detail.component';
import { LoadingSpinnerComponent } from './Spinner/loading-spinner/loading-spinner.component';
import { ProfileComponent } from './User/profile/profile.component';
import { UserEditComponent } from './User/user-edit/user-edit.component';
import { UserPostComponent } from './User/user-post/user-post.component';
import { ViewOnlyPublicPostComponent } from './User/view-only-public-post/view-only-public-post.component';
import { ViewProfileComponent } from './User/view-profile/view-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponentComponent,
    ResetpasswordComponent,
    VerifyMailComponent,
    NoSanitizePipe,
    HomeComponent,
    UCreatePostComponent,
    UpostDetailComponent,
    LoadingSpinnerComponent,
    ProfileComponent,
    UserEditComponent,
    UserPostComponent,
    ViewOnlyPublicPostComponent,
    ViewProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
